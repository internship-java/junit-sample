package md.codefactory.sandul_dmitriy.impl;

import md.codefactory.denisc.MockInterface;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DimaSSimpleImpl implements MockInterface {

    private final static Logger LOG = Logger.getLogger(DimaSSimpleImpl.class.getName());
    @Override
    public void foo() {
        LOG.info("entering foo method");
        System.out.println("Hello I'am Java Developer !!!");
        LOG.log(Level.WARNING, "exiting foo() method");
    }
}
