package md.codefactory.sandul_dmitriy.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;

public class JsonMapper {
    private ObjectMapper mapper = new ObjectMapper();

    public String userToJson(User user) throws JsonProcessingException {

        String userAsString = mapper.writeValueAsString(user);

        return userAsString;
    }

    public User userFromJson(String json) throws Exception{

        User user = mapper.readValue(json, User.class);
        return user;
    }
}
