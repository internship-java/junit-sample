package md.codefactory.igor;

import md.codefactory.denisc.MockInterface;


public class IgorImplements implements MockInterface {

    @Override
    public void foo() {
        System.out.println("igor implements");
    }

}
