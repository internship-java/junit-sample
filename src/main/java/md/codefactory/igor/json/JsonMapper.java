package md.codefactory.igor.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;
import java.io.File;
import java.io.IOException;

public class JsonMapper {

    private ObjectMapper mapper = new ObjectMapper();

    public String userToJson(User user) throws JsonProcessingException {


        String jsonObject = mapper.writeValueAsString(user);
        System.out.println(jsonObject);
        return jsonObject;

    }

    public User userFromJson(String json) throws IOException {


        User user = mapper.readValue(json, User.class);

        return user;
    }
}
