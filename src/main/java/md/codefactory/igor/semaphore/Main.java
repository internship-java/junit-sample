package md.codefactory.igor.semaphore;

import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) {
        Semaphore s1 = new Semaphore(1);
        Semaphore s2 = new Semaphore(1);

        Thread t = new Thread(new DoubleResourceGrable(s1, s2));

        Thread t2 = new Thread(new DoubleResourceGrable(s2, s1));

        t.start();
        t2.start();

        try {
            t.join();
            t2.join();
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }
}
