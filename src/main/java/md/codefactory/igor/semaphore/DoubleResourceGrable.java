package md.codefactory.igor.semaphore;

import java.util.concurrent.Semaphore;

public class DoubleResourceGrable implements Runnable {

    private Semaphore first;
    private Semaphore second;

    public DoubleResourceGrable(Semaphore s1, Semaphore s2) {
        first = s1;
        second = s2;
    }

    public void run () {
        try {
            Thread t = Thread.currentThread();
            first.acquire();
            System.out.println(t + "acquired s1 " + first);

            Thread.sleep(200);

            second.acquire();
            System.out.println(t + " acquirde s1 " + first);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
