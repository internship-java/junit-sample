//package md.codefactory.igor.semaphore;
//
//import java.util.concurrent.CompletableFuture;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeUnit;
//
//import static java.util.concurrent.CompletableFuture.supplyAsync;
//import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;
//
//public class FutureApp {
//
//
//    public class FutureApp {
//
//        public static void main(String[] args) {
//
//
//
//            CompletableFuture<String> welcomeText = supplyAsync(() -> {
//
//                try {
//
//                    TimeUnit.SECONDS.sleep(1);
//
//                    System.out.println("First method execution");
//
//                } catch (InterruptedException e) {
//
//                    throw new IllegalStateException(e);
//
//                }
//
//                return "Interns";
//
//            }).thenApply(name -> {
//
//                return "Hello " + name;
//
//            }).thenApply(greeting -> {
//
//                return greeting + ", welcome to the CF lessons";
//
//            }).exceptionally(ex -> {
//
//                System.out.println("Oops! We have an exception - " + ex.getMessage());
//
//                return "Unknown!";
//
//            });
//
//            futureResult(welcomeText);
//
//            futureResult(welcomeText);
//
//
//            //Composition
//
//            CompletableFuture<String> completableFuture = supplyAsync(() -> "Hello")
//
//                    .thenCompose(s -> supplyAsync(() -> s + " World"));
//
//            futureResult(completableFuture);
//
//            CompletableFuture<String> combineFuture = supplyAsync(() -> "Hello")
//
//                    .thenCombine(supplyAsync(
//
//                            () -> " World"), (s1, s2) -> s1 + s2);
//
//            int n = 9;
//            CompletableFuture<Double> result = factorial(n).thenCombine(log new(Double(n)), (f, l) -> f+l)
//        .thenApply(partial -> partial +7)
//                    .thenApply(res -> "Result = " + res);
//            futureResult(result);
//
//
//        }
//        public static void CompletabldeFuture<Double> log (Double d) {
//            return CompletableFuture.supplyAsync(() -> Math.log10(d));
//
//        }
//
//        private static int recursiveFactorial(int n) {
//            if(n <= 1) return 1;
//            return recursiveFactorial(n-1);
//
//        }
//
//        public static void CompletabldeFuture<Double> factorial(int n {
//            return CompletableFuture.supplyAsync(() ->new Double(recursiveFactorial(n)));
//        }
//
//
//
//
//        private static void futureResult(CompletableFuture<String> text) {
//
//
//            try {
//
//                System.out.println(text.get());
//
//            } catch (InterruptedException e) {
//
//                e.printStackTrace();
//
//            } catch (ExecutionException e) {
//
//                e.printStackTrace();
//
//            }
//
//        }
//
//
//    }
//}
