package md.codefactory.samaia;

import md.codefactory.denisc.MockInterface;

public class SamaiaImpl implements MockInterface {

    @Override
    public void foo() {
        System.out.println("Hello, Samaia here!");
    }
}
