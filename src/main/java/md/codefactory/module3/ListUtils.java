package md.codefactory.module3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListUtils {

    private final List<String> stringsList;

    public ListUtils(List<String> stringsList) {
        this.stringsList = stringsList;
    }

    public void reverseStrings() {
        //reverse the items order from the list
        Collections.reverse(stringsList);
    }

    public void allToUppercase() {
        for (int i = 0; i < stringsList.size(); i++) {
            String item = stringsList.get(i);
            stringsList.set(i, item.toUpperCase());
        }
    }

    public void fistAndLastToUpperCase() {
        //change value for item on index 0
        String item = stringsList.get(0);
        stringsList.set(0, item.toUpperCase());
        // last index update
        int lastIndex = stringsList.size() - 1;
        String lastItem = stringsList.get(lastIndex);
        stringsList.set(lastIndex, lastItem.toUpperCase());
    }
}
