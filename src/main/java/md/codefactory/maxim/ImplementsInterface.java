package md.codefactory.maxim;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ImplementsInterface implements MokInterface {

    private final static Logger LOG = Logger.getLogger(ImplementsInterface.class.getName());

    public static void main(String[] args) {

        ImplementsInterface implementsInterface = new ImplementsInterface();
        implementsInterface.foo();

    }

    @Override
    public void foo() {
        LOG.info("entering foo method");
        System.out.println("Maxim");
        LOG.log(Level.WARNING, "exiting foo method");
    }



}