package md.codefactory.maxim.multithread;

import java.util.concurrent.Semaphore;

public class DoubleResourceGrabber implements Runnable {

    private Semaphore first;
    private Semaphore second;

    public DoubleResourceGrabber(Semaphore s1, Semaphore s2) {
        this.first = s1;
        this.second = s2;
    }

        public void run() {
        try {
            Thread t = Thread.currentThread();
            first.acquire();
            System.out.println(t + " acquire s1 " + first);

            Thread.sleep(200);

            second.acquire();
            System.out.println(t + " acquire s2 " + second);

            second.release();
            System.out.println(t + " released s2 " + second);

            first.release();
            System.out.println(t + " released s1 " + first);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }

}


