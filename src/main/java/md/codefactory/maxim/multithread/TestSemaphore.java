package md.codefactory.maxim.multithread;

// java program to demonstrate
// use of semaphores Locks

import java.util.concurrent.*;

//A shared resource/class.
class Shared {
    static int count = 0;
}

class TestSemaphore extends Thread {
    Semaphore sem;
    String threadName;

    public TestSemaphore(Semaphore sem, String threadName) {
        super(threadName);
        this.sem = sem;
        this.threadName = threadName;
    }

    @Override
    public void run() {
        // run by thread t1
        if (this.getName().equals("t2")) {
            System.out.println("Starting " + threadName);
            try {
                sem.acquire();
            } catch (InterruptedException exc) {
                System.out.println(exc);
            }
            sem.release();
        }

        // run by thread t2

            /*System.out.println("Starting " + threadName);
            try {
                sem.acquire();

            } catch (InterruptedException exc) {
                System.out.println(exc);
            }
            sem.release();*/

    }
}

// Driver class
class SemaphoreDemo {
    public static void main(String args[]) throws InterruptedException {

        Semaphore sem = new Semaphore(1);

        TestSemaphore t1 = new TestSemaphore(sem, "t1");
        TestSemaphore t2 = new TestSemaphore(sem, "t2");

        // stating threads t1 and t2
        t1.start();
        t2.start();

        // waiting for threads t1 and t2
        t1.join();
        t2.join();

    }
}

