package md.codefactory.maxim.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;

import java.io.IOException;

public class JsonMapper {

    private ObjectMapper mapper = new ObjectMapper();

    public String userToJson(User user) throws JsonProcessingException {

        //ObjectMapper mapper = new ObjectMapper();
        String jsonObject = mapper.writeValueAsString(user);
        return jsonObject;
    }

    public User userFromJson(String json) throws IOException {
        User user = mapper.readValue(json, User.class);
        return user;
    }
}
