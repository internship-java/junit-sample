package md.codefactory.alionac.threads.homework.var4;
import java.util.concurrent.CountDownLatch;


public class Tail4Thread extends Thread {
    private final CountDownLatch waitLatch;

    public Tail4Thread(CountDownLatch waitLatch, String name) {
        this.waitLatch = waitLatch;
        setName(name);
    }

    public void run() {

        try {
            System.out.println(Thread.currentThread().getName() + " waiting for first signal...");
            waitLatch.await();
            System.out.println(Thread.currentThread().getName() + " received the first signal...");
            System.out.println(Thread.currentThread().getName() + " waiting for second signal...");
            System.out.println("    " + Thread.currentThread().getName() + " executed...");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
