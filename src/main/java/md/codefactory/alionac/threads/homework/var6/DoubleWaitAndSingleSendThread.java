package md.codefactory.alionac.threads.homework.var6;

import java.util.concurrent.CountDownLatch;

public class DoubleWaitAndSingleSendThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch waitLatch2;
    private final CountDownLatch signalLatch;

    public DoubleWaitAndSingleSendThread(CountDownLatch waitLatch, CountDownLatch waitLatch2, CountDownLatch signalLatch, String name) {
        this.waitLatch = waitLatch;
        this.waitLatch2 = waitLatch2;
        this.signalLatch = signalLatch;
        setName(name);
    }

    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " waiting for first signal...");
            waitLatch2.await();
            System.out.println(Thread.currentThread().getName() + " received the first signal...");
            System.out.println(Thread.currentThread().getName() + " waiting for second signal...");
            waitLatch.await();
            System.out.println(Thread.currentThread().getName() + " received the second signal...");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("    " + Thread.currentThread().getName() + " executed...");
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        signalLatch.countDown();
    }


}

