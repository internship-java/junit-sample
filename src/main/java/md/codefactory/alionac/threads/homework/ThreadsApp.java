package md.codefactory.alionac.threads.homework;

import java.util.concurrent.Semaphore;

public class ThreadsApp {

    public static void main(String args[]) {

        Semaphore s1 = new Semaphore(0);
        Semaphore s2 = new Semaphore(0);

        Thread t1 = new Thread(new ThreadsImpl(s1,s2));
        System.out.println("new Thread " + t1);

        Thread t2 = new Thread(new ThreadsImpl(s1,s2));
        System.out.println("new Thread " + t2);


        t1.start();
        t2.start();


        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        System.out.println("Main thread exiting.");
    }

}
