package md.codefactory.alionac.threads.homework.var6;
import java.util.concurrent.CountDownLatch;


public class TailThread extends Thread {
    private final CountDownLatch waitLatch;
    private final CountDownLatch waitLatch2;

    public TailThread(CountDownLatch waitLatch, CountDownLatch waitLatch2, String name) {
        this.waitLatch = waitLatch;
        this.waitLatch2 = waitLatch2;
        setName(name);
    }

    public void run() {

        try {
            System.out.println(Thread.currentThread().getName() + " waiting for first signal...");
            waitLatch.await();
            System.out.println(Thread.currentThread().getName() + " received the first signal...");
            System.out.println(Thread.currentThread().getName() + " waiting for second signal...");
            waitLatch2.await();
            System.out.println(Thread.currentThread().getName() + " received the second signal...");
            System.out.println("    " + Thread.currentThread().getName() + " executed...");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
