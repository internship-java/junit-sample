package md.codefactory.alionac.threads.homework;

import md.codefactory.alionac.threads.homework.var4.Head4Thread;
import md.codefactory.alionac.threads.homework.var4.SingleWaitAndSingleSend4Thread;
import md.codefactory.alionac.threads.homework.var4.Tail4Thread;

import java.util.concurrent.CountDownLatch;

public class Multithread4App {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch ALatch = new CountDownLatch(2);
        CountDownLatch CLatch = new CountDownLatch(1);


        new Tail4Thread(CLatch,"Thread-4").start();
        new Head4Thread(ALatch,  "Thread-2").start();
        new SingleWaitAndSingleSend4Thread(ALatch, CLatch, "Thread-3").start();
        new Head4Thread(ALatch, "Thread-1").start();



    }
}
