package md.codefactory.alionac.threads.homework;

import md.codefactory.alionac.threads.homework.var6.DoubleWaitAndSingleSendThread;
import md.codefactory.alionac.threads.homework.var6.HeadThread;
import md.codefactory.alionac.threads.homework.var6.SingleWaitAndSingleSendThread;
import md.codefactory.alionac.threads.homework.var6.TailThread;

import java.util.concurrent.CountDownLatch;

public class Multithread6App {
    public static void main(String[] args) {
        CountDownLatch ALatch = new CountDownLatch(1);
        CountDownLatch BLatch = new CountDownLatch(1);
        CountDownLatch CLatch = new CountDownLatch(1);
        CountDownLatch DLatch = new CountDownLatch(1);
        CountDownLatch ELatch = new CountDownLatch(1);


        new TailThread(DLatch, ELatch, "Thread-6").start();
        new SingleWaitAndSingleSendThread(ALatch, BLatch, "Thread-2").start();
        new DoubleWaitAndSingleSendThread(ALatch, BLatch, CLatch, "Thread-3").start();
        new DoubleWaitAndSingleSendThread(BLatch, CLatch, DLatch, "Thread-4").start();
        new SingleWaitAndSingleSendThread(DLatch, ELatch, "Thread-5").start();
        new HeadThread(ALatch, "Thread-1").start();

    }
}
