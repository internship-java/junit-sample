package md.codefactory.alionac.threads.homework.var6;

import java.util.concurrent.CountDownLatch;

public class HeadThread extends Thread {
    private final CountDownLatch latch;

    public HeadThread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        System.out.println("    " + Thread.currentThread().getName() + " executed...");
        latch.countDown();
    }
}