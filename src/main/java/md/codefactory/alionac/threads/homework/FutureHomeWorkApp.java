package md.codefactory.alionac.threads.homework;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class FutureHomeWorkApp {
    public static void main(String[] args) {

        // log(n!) + 7
        int n = 9;

        CompletableFuture<String> result = CompletableFuture.supplyAsync(() -> factorial(n))
                .thenCompose(f -> log(f))
                .thenApply(partial -> partial + 7)
                .thenApply(res -> "Result " + res);

        futureResult(result);
    }

    private static Double factorial(int n) {
        if ((n <= 0) || (n == 1)) {
            return 1.;
        }
        return n * factorial(n - 1);
    }

    private static CompletableFuture<Double> log(Double d) {
        return CompletableFuture.supplyAsync(() -> Math.log10(d));
    }

    private static void futureResult(CompletableFuture<String> text) {
        try {
            System.out.println(text.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
