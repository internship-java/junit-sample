package md.codefactory.alionac.threads.homework;

import java.util.concurrent.Semaphore;

public class ThreadsImpl implements Runnable {

    String name;
    private Semaphore first;
    private Semaphore second;

    public ThreadsImpl(Semaphore s1, Semaphore s2) {
        first = s1;
        second = s2;
    }

    @Override
    public void run() {
        try {

            Thread t = Thread.currentThread();

            first.acquire();
            System.out.println(t + " acquired s1 " + first);

            Thread.sleep(200);

            second.acquire();
            System.out.println(t + " acquired s2 " + second);

            second.release();
            System.out.println(t + " released s2 " + second);

            first.release();
            System.out.println(t + " released s1 " + first);
        } catch (InterruptedException e) {
            System.out.println(name + "Interrupted");
        }
        System.out.println(name + " exiting.");
    }
}
