package md.codefactory.alionac.threads.homework.var4;

import java.util.concurrent.CountDownLatch;

public class Head4Thread extends Thread {
    private final CountDownLatch latch;

    public Head4Thread(CountDownLatch latch, String name) {
        this.latch = latch;
        setName(name);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + " sending the signal...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("    " + Thread.currentThread().getName() + " executed...");
        latch.countDown();
    }
}