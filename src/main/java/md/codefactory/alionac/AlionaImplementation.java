package md.codefactory.alionac;

import md.codefactory.denisc.MockInterface;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AlionaImplementation implements MockInterface {

    private final static Logger LOG = Logger.getLogger(AlionaImplementation.class.getName());

    @Override
    public void foo() {
        LOG.info("entering foo() method");
        System.out.println("Hello Aliona");
        LOG.log(Level.WARNING,"exiting foo() method");
    }
}
