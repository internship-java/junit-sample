package md.codefactory.polevoi_vadim;

import md.codefactory.denisc.MockInterface;

public class MockInterfaceImpl implements MockInterface {

    @Override
    public void foo() {
        System.out.println("Now it's not a mock interface!");
    }
}
