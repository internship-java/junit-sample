package md.codefactory.polevoi_vadim.multithreading;

import java.util.concurrent.Semaphore;

public class MultithreadApp {

    public static void main(String[] args) {
        Semaphore s1 = new Semaphore(1);
        Semaphore s2 = new Semaphore(1);

        Thread t1 = new Thread(new DoublerResourceGrabber(s1, s2));
        Thread t2 = new Thread(new DoublerResourceGrabber(s2, s1));

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
