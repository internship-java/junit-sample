package md.codefactory.polevoi_vadim.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;

import java.io.IOException;

public class JsonMapper {

    private static ObjectMapper mapper = new ObjectMapper();

    public String userToJSON(User user) throws JsonProcessingException {
        return mapper.writeValueAsString(user);
    }

    public User userFromJSON(String json) throws IOException {
        return mapper.readValue(json, User.class);
    }

}
