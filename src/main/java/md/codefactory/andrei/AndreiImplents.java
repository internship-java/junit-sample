package md.codefactory.andrei;

import md.codefactory.denisc.MockInterface;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AndreiImplents implements MockInterface {
    //reflection
    private final static Logger LOG = Logger.getLogger(AndreiImplents.class.getName());
    @Override
    public void foo() {
        LOG.info("enter in foo method");
        System.out.println("Hello Andrei");
        LOG.log(Level.WARNING,"exiting foo() method");
    }
}
