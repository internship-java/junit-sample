package md.codefactory.andrei.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;

import java.io.IOException;

public class JsonMapper {


    private ObjectMapper objectMapper = new ObjectMapper();
    public    String userToJson(User user) throws JsonProcessingException {

        String jsonObject = objectMapper.writeValueAsString(user);
        return jsonObject;
    }
    public User userFromJson(String json) throws IOException {

        User user = objectMapper.readValue(json,User.class);
        return user;
    }

}
