package md.codefactory.module2;

public class Physics {

    public static double getSpeed(double distance, int timeInSec) {
        double speed = distance / timeInSec;
        return speed;
    }

}
