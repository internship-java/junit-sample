package md.codefactory.module2;

import java.util.ArrayList;
import java.util.List;

public class NumbersUtil {

    public static List<Integer> sublistOccurences(List<Integer> allNumbers, int number) {
        List<Integer> repeatsList = new ArrayList<Integer>();
        for (int i : allNumbers) {
            if (i == number) {
                //add to repeatsList
                repeatsList.add(i);
            }
        }
        return repeatsList;
    }

}
