package md.codefactory.danr;

import md.codefactory.denisc.MockInterface;

public class MockInterfaceImpl implements MockInterface {

    @Override
    public void foo() {
        System.out.println("hello, world");
        System.out.println("this is an ordinary interface implementation");
    }
}
