package md.codefactory.ionnegara;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MockImpl implements MockInterface {

    private final static Logger LOG = Logger.getLogger(MockImpl.class.getName());

    @Override
    public void foo() {
        LOG.info("entering foo method");
        System.out.println("Hello!");
        LOG.log(Level.WARNING, "exiting foo method");
    }
}
