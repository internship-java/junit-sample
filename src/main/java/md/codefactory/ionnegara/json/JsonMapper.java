package md.codefactory.ionnegara.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;

public class JsonMapper {
    public String userToJson(User user) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = mapper.writeValueAsString(user);
        return jsonObject;
        // string starts with "{"id"
    }
}
