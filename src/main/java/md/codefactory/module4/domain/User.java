package md.codefactory.module4.domain;

public class User {

    private int id;
    private String fullName;
    private String userName;
    private String password;

    public User() {
    }

    public User(String userName) {
        this.userName = userName;
    }

    public User(String fullName, String userName) {
        this.fullName = fullName;
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
