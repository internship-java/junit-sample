package md.codefactory.module4.service;

import md.codefactory.module4.domain.User;
import md.codefactory.module4.exeptions.InvalidUsernameOrPasswordException;
import md.codefactory.module4.repository.UserRepository;

public class UserService {

    private final UserRepository userRepository;


    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

   public boolean authenticate(String username,String password) throws InvalidUsernameOrPasswordException {
       User dbUser = userRepository.findOneByUsername(username);
       if(dbUser==null){
           throw new InvalidUsernameOrPasswordException("No user found");
       }

       if(!dbUser.getPassword().equals(password)){
           throw new InvalidUsernameOrPasswordException("No user found");
       }
       return true;
   }
}
