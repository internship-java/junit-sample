package md.codefactory.module4.exeptions;

public class UserAlreadyExistsExeption extends Exception {
    public UserAlreadyExistsExeption(String message) {
        super(message);
    }
}
