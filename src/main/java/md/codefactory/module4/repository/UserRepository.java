package md.codefactory.module4.repository;

import md.codefactory.module4.domain.User;
import md.codefactory.module4.exeptions.UserAlreadyExistsExeption;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepository {

    private List<User> users = new ArrayList<User>();

    public void saveUser(User newUser) throws UserAlreadyExistsExeption {
        // first check if there is no user with this username
        String userName = newUser.getUserName();
        User currentUser = findOneByUsername(userName);
        if (currentUser != null) {
            throw new UserAlreadyExistsExeption("Sorry, username " + userName + " already exists");
        }
        //store in database
        users.add(newUser);
    }

    public User findOneByUsername(String userName) {
        for (User u : users) {
            if (u.getUserName().equals(userName)) {
                // we found a user with this username
                return u;
            }
        }
        return null;
    }

    public List<User> findAllByFullNameStartsWith(final String prefix) {
        return users.stream()
                .filter(user -> user.getFullName() != null)
                .filter(user -> user.getFullName()
                        .startsWith(prefix))
                .collect(Collectors.toList());
    }

    /*
    count all users from DB
     */
    public int countUsers() {
        return users.size();
    }

}