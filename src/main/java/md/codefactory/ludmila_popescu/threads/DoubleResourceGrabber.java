package md.codefactory.ludmila_popescu.threads;

import java.util.concurrent.Semaphore;

public class DoubleResourceGrabber implements Runnable {


    private Semaphore first;
    private Semaphore second;
    public DoubleResourceGrabber(Semaphore s1, Semaphore s2) {
        first=s1;
        second=s2;
    }

    @Override
    public void run() {
        try{
            Thread t= Thread.currentThread();
            first.acquire();
            System.out.println(t+ " acuire s1 "+first);
          // Thread.sleep(200);


            second.acquire();
            System.out.println(t+ " acuire s2 "+second);

            second.release();
            System.out.println(t+ " release s2 "+ second);
            first.release();
            System.out.println(t+ " release s1 "+ first);

        }catch (InterruptedException ex){
            ex.printStackTrace();
        }
        System.out.println("we got lucky");
    }
}
