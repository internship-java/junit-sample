package md.codefactory.ludmila_popescu.threads;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class FutureApHomework {


    public static void main(String[] args) {


        // n!+log n + 7
        int n = 9;
        CompletableFuture<String> result = factorial(n)
                .thenCombine(log(new Double(n)), (f, l) -> f + l)
                .thenApply(partial -> partial + 7)
                .thenApply(res -> "Result (n!+log n + 7) = " + res);

        futureResult(result);

        //log(n!)+7   TODO
        CompletableFuture<String> result2 = factorial(n)
                .thenCompose(l -> log(new Double(l)))
                .thenApply(partial -> partial + 7)
                .thenApply(res -> "Result    (log(n!)+7) = " + res);
        futureResult(result2);

    }

    private static CompletableFuture<Double> log(Double d) {
        return CompletableFuture.supplyAsync(() -> Math.log10(d));
    }

    private static int recursiveFactorial(int n) {
        if (n <= 1) return 1;
        return n * recursiveFactorial(n - 1);
    }

    private static CompletableFuture<Double> factorial(int n) {
        return CompletableFuture.supplyAsync(() -> new Double(recursiveFactorial(n)));
    }

    private static void futureResult(CompletableFuture<String> text) {
        try {
            System.out.println(text.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
