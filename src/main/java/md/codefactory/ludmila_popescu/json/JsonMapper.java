package md.codefactory.ludmila_popescu.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.module4.domain.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonMapper {

    private  ObjectMapper mapper =new ObjectMapper();
    public String userToJson(User user) throws JsonProcessingException {
        String jsonObject = mapper.writeValueAsString(user);
        return jsonObject;
        // string starts with "{"id"
    }

    public User userFromJson(String json) throws IOException {
        User user = mapper.readValue(json, User.class);
        return user;
    }


}
