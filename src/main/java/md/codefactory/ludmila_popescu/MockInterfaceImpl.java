package md.codefactory.ludmila_popescu;

import md.codefactory.denisc.MockInterface;

public class MockInterfaceImpl implements MockInterface {

    @Override
    public void foo() {
        System.out.println("hello, world");
    }
}
