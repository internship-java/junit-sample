package md.codefactory.denisc;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MockImpl implements MockInterface {

    private final static Logger LOG
            = Logger.getLogger(MockImpl.class.getName());

    @Override
    public void foo() {
        LOG.info("entering foo method");
        System.out.println("Hello Denis");
        LOG.log(Level.WARNING, "exiting foo() method");
    }
}
