package md.codefactory.denisc;

public interface MockInterface {

    /**
     * please in the implementation
     * show an message to output ex:
     *  Hello {yourName}
     */

    void foo();

}
