package md.codefactory.denisc.json;

import md.codefactory.module4.domain.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonMapperTest {

    private final JsonMapper jsonMapper = new JsonMapper();

    @Test
    public void userToJson() throws Exception {
        User user = new User();
        user.setId(1);
        user.setUserName("demouser");
        String json = jsonMapper.userToJson(user);
        //  {"id":1}
        assertTrue(json.startsWith("{\"id\":1"));
    }

    @Test
    public void jsonoUserTest() throws Exception{
        String json = "{\"id\":10,\"fullName\":\"John\"}                ";
        User user = jsonMapper.userFromJson(json);
        assertEquals(10,user.getId());
        assertEquals("John",user.getFullName());
        //todo continue home
    }
}
