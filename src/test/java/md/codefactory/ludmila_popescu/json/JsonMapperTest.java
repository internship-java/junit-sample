package md.codefactory.ludmila_popescu.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.module4.domain.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonMapperTest {
    private final JsonMapper mapper = new JsonMapper();

    @Test
    public void userToJsonTest() throws JsonProcessingException {
        User user = new User();
        user.setId(1);
        user.setUserName("demouser");
        String json = mapper.userToJson(user);
        assertTrue(json.startsWith("{\"id\":1"));
    }

    @Test
    public void userFromJsonTest() throws Exception {
        String json = "{\"id\":10,\"fullName\":\"John\",\"userName\":\"Doe\",\"password\":\"password\"}";
        User user = mapper.userFromJson(json);
        assertEquals(10, user.getId());
        assertEquals("John", user.getFullName());
        assertEquals("Doe", user.getUserName());
        assertEquals("password", user.getPassword());

    }

    @Test
    public void userToJsonTestContains() throws JsonProcessingException {
        User user = new User();
        user.setId(1);
        user.setUserName("demouser");
        String json = mapper.userToJson(user);
        System.out.println(json);
        assertTrue(json.contains("\"userName\":\"demouser\""));
        assertTrue(json.contains("\"fullName\":\"fullName\""));
    }
    @Test
    public void userToJsonTestCompareStrings() throws JsonProcessingException {
        String expected = "{\"id\":1,\"fullName\":\"John\",\"userName\":\"Doe\",\"password\":\"password\"}";
        User user = new User();
        user.setId(1);
        user.setUserName("Doe");
        user.setFullName("John");
        user.setPassword("password");
        String json = mapper.userToJson(user);
        System.out.println(json);
        assertEquals(expected,json);

    }
}