package md.codefactory.andrei.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.module4.domain.User;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class JsonMapperTest {

    private final JsonMapper jsonMapper = new JsonMapper();

    @Test
    public void userToJson() throws JsonProcessingException {
        User user = new User("andrei copaceanu", "andrei");
        JsonMapper jsonMapper = this.jsonMapper;
        String result =  jsonMapper.userToJson(user);
        Assert.assertTrue(result.startsWith("{"));
    }

    @Test
    public void userFromJson() throws IOException {
        String json = "{\"id\":10},\"fullName\":\"John\"}              ";
        User user  = jsonMapper.userFromJson(json);
        Assert.assertEquals(10,user.getId());
        // to do continue home
    }
}