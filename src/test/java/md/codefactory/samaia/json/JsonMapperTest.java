package md.codefactory.samaia.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.module4.domain.User;
import org.junit.Assert;
import org.junit.Test;

public class JsonMapperTest {

    private final JsonMapper jsonMapper = new JsonMapper();

    @Test
    public void string_Starts_With_Test() throws JsonProcessingException {
        User user = new User();
        user.setId(1);
        user.setUserName("demouser");
        String json =  jsonMapper.userToJson(user);
        Assert.assertTrue(json.startsWith("{\"id\":1"));
    }

    @Test
    public void json_User_Test() throws Exception{
        String json = "{\"id\":10,\"fullName\":\"John\"}    ";
        User user = jsonMapper.userFromJson(json);
        Assert.assertEquals(10, user.getId());
        Assert.assertEquals("John", user.getFullName());
    }
}
