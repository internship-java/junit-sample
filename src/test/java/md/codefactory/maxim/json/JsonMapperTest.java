package md.codefactory.maxim.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class JsonMapperTest {

    private final JsonMapper jsonMapper = new JsonMapper();

    @Test
    public void userToJson() throws JsonProcessingException {
        User user = new User();
        user.setId(1);
        user.setUserName("demouser");
        // JsonMapper jsonMapper = new JsonMapper();
        String json = jsonMapper.userToJson(user);
        Assert.assertTrue(json.startsWith("{\"id\":1"));
    }

    @Test
    public void jsonUserTest() throws Exception{
        String json = "{\"id\":10,\"fullName\":\"John\"}";
        User user = jsonMapper.userFromJson(json);
        assertEquals(10,user.getId());
        assertEquals("John",user.getFullName());
        //todo
    }

}