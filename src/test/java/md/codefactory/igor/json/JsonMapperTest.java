package md.codefactory.igor.json;

import md.codefactory.module4.domain.User;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonMapperTest {


    private final JsonMapper jsonMapper = new JsonMapper();

    @Test
    public void userToJsonTest() throws  Exception{

        User user = new User();
        user.setId(1);
        user.setUserName("demo");

        String jsonMapper = this.jsonMapper.userToJson(user);

        Assert.assertTrue(jsonMapper.startsWith("{\"id\":1"));

    }

    @Test
    public void userFromJsonTest() throws Exception{

        String json = "{\"id\":10, \"fullName\":\"John\"}         ";
        User user = jsonMapper.userFromJson(json);
        Assert.assertEquals(10,user.getId());
        Assert.assertEquals(10,user.getFullName());

    }
}