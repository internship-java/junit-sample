package md.codefactory.alionac.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.module4.domain.User;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class JsonMapperTest {

    User user;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setFullName("Mock name");
        user.setId(123455);
        user.setPassword("buburuza");
        user.setUserName("mock");
    }

    @Test
    public void userToJson() throws JsonProcessingException {
        JsonMapper jsonMapper = new JsonMapper();
        String result = jsonMapper.userToJson(user);
        System.out.println(result);
        assertTrue(result.startsWith("{\"id\":123455"));
    }

    @Test
    public void userFromJson() throws Exception {
        String json = "{\"id\":123455,\"fullName\":\"Mock name\",\"userName\":\"mock\",\"password\":\"buburuza\"}";
        User user = new JsonMapper().userFromJson(json);
        assertEquals(123455,user.getId());
        assertEquals("Mock name",user.getFullName());
        assertEquals("mock",user.getUserName());
        assertEquals("buburuza",user.getPassword());
    }
}