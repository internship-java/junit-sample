package md.codefactory.olgas.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.module4.domain.User;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class jsonMapperTest {


    @Test
    public void userToJsonTest() throws Exception {
        User user = new User();
        user.setId(1);
        user.setUserName("user1");
        String json = new jsonMapper().userToJson(user);
        Assert.assertTrue(json.startsWith("{\"id\":1"));

    }


}