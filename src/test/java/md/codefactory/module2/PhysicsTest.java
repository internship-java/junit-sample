package md.codefactory.module2;

import org.junit.Assert;
import org.junit.Test;

public class PhysicsTest {

    @Test
    public void getSpeedTest_1() throws Exception {
        double distance = 10;
        int timeInSec = 5;
        double expectedSpeed = 2.0;
        double actualSpeed = Physics.getSpeed(distance, timeInSec);
        //check if expectedSpeed == actualSpeed !
        Assert.assertEquals(expectedSpeed, actualSpeed, 0);
    }

    @Test
    public void getSpeedTest_2() throws Exception {
        double distance = 5;
        int timeInSec = 5;
        double expectedSpeed = 1.0;
        double actualSpeed = Physics.getSpeed(distance, timeInSec);
        //check if expectedSpeed == actualSpeed !
        Assert.assertEquals(expectedSpeed, actualSpeed, 0);
    }
}
