package md.codefactory.module2;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NumbersUtilTest {
    /*
    all test methods should be public void,
    and to throw Exception
     */
    @Test
    public void sublistOccurencesTest_1() {
        List<Integer> sourceArrayList = new ArrayList<Integer>();
        sourceArrayList.add(10);
        sourceArrayList.add(2);
        sourceArrayList.add(8);
        sourceArrayList.add(8);
        sourceArrayList.add(2);

        List<Integer> result = NumbersUtil.sublistOccurences(sourceArrayList, 8);
        //check for result TO NOT BE NULL
        Assert.assertNotNull(result);
        // TO NOT BE EMPTY
        Assert.assertFalse(result.isEmpty());//-> false == isEmpty()
        //check list size to be 2
        Assert.assertEquals(2, result.size());
        //check for an item on index 0
        Assert.assertEquals(Integer.valueOf(8), result.get(0));
    }


    @Test
    public void sublistOccurencesTest_2() {
        List<Integer> sourceArrayList = Arrays.asList(10, 2, 8, 8, 2);
        List<Integer> result = NumbersUtil.sublistOccurences(sourceArrayList, 2);
        //check for result TO NOT BE NULL
        Assert.assertNotNull(result);
        // TO NOT BE EMPTY
        Assert.assertFalse(result.isEmpty());//-> false == isEmpty()
        //check list size to be 2
        Assert.assertEquals(2, result.size());
        //check for an item on index 0
//        Assert.assertEquals(Integer.valueOf(2), result.get(0));
        Assert.assertSame(2 , result.get(0));
    }

}
