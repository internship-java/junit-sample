package md.codefactory.module4.repository;

import md.codefactory.module4.domain.User;
import md.codefactory.module4.exeptions.UserAlreadyExistsExeption;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class UserRepositoryTest {

    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        userRepository = new UserRepository();
        User user = new User("mockUsername");
        userRepository.saveUser(user);
    }

    @Test
    public void save_new_user_test() throws Exception {
        int oldCount = userRepository.countUsers();
        User user = new User("demoUser1");
        userRepository.saveUser(user);
        int currentCount = userRepository.countUsers();
        //   oldCount < currentCount
        Assert.assertTrue(oldCount < currentCount);
        Assert.assertEquals(currentCount, oldCount + 1);
    }

    @Test(expected = UserAlreadyExistsExeption.class)
    public void save_new_already__existing_user() throws Exception {
        User user = new User("mockUsername");
        userRepository.saveUser(user);
    }

    @Test
    public void findAllByFullNameStartsWith() throws Exception {
        userRepository.saveUser(new User("Ion","u1"));
        userRepository.saveUser(new User("Ionel","u2"));
        userRepository.saveUser(new User("Ionica","u3"));
        userRepository.saveUser(new User("Gicu","u4"));
        List<User> foundUsers = userRepository.findAllByFullNameStartsWith("Ion");
        assertFalse(foundUsers.isEmpty());
        assertEquals(3,foundUsers.size());
    }
}