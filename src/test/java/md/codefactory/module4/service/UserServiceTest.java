package md.codefactory.module4.service;

import md.codefactory.module4.domain.User;
import md.codefactory.module4.exeptions.InvalidUsernameOrPasswordException;
import md.codefactory.module4.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class
UserServiceTest {

    private UserRepository userRepository;
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userRepository = new UserRepository();
        User u1 = new User("admin");
        u1.setPassword("admin");
        userRepository.saveUser(u1);

        userService = new UserService(userRepository);
    }

    @Test
    public void authenticate_ok() throws Exception {
        boolean isAuthenticated = userService
                .authenticate("admin", "admin");
        assertTrue(isAuthenticated);
    }

    @Test(expected = InvalidUsernameOrPasswordException.class)
    public void authenticate_not_ok() throws Exception {
        userService
                .authenticate("admin", "invalid");

    }
}