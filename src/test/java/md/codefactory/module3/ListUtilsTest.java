package md.codefactory.module3;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ListUtilsTest {

    private ListUtils listUtils;
    private List<String> items;

    @Before
    public void setUp() throws Exception {
        items = new ArrayList<String>();
        items.add("s1");
        items.add("s2");
        items.add("s3");
        items.add("s4");
        listUtils = new ListUtils(items);
    }


    @Test
    public void reverseStrings() {
        listUtils.reverseStrings();
        // [s4,s3,s2,s1]
        assertNotNull(items);
        assertEquals("s4", items.get(0));
        assertEquals("s1", items.get(3));
    }

    @Test
    public void allToUppercase() {
        listUtils.allToUppercase();
        assertNotNull(items);
        assertEquals("S1", items.get(0));
        assertEquals("S4", items.get(3));
    }

    @Test
    public void firstAndLasToUppercase(){
        listUtils.fistAndLastToUpperCase();
        assertFalse(items.isEmpty());
        assertEquals("S1",items.get(0));
        assertEquals("s2",items.get(1));
        assertEquals("S4", items.get(3));
    }
}