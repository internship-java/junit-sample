package md.codefactory.polevoi_vadim.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.module4.domain.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class JsonMapperTest {

    private JsonMapper jsonMapper;
    private User user;

    @Before
    public void setUp() {
        jsonMapper = new JsonMapper();
        user = new User("John Doe", "john.doe");
    }

    @Test
    public void userToJSON() throws JsonProcessingException {
        String json = jsonMapper.userToJSON(user);
        boolean isJSON = json.startsWith("{\"id\"");

        assertTrue(isJSON);
    }

    @Test
    public void userFromJSON() throws IOException {
        String json = "{\"id\":10, \"fullName\":\"John Doe\",\"userName\":\"john.doe\",\"password\":\"pass123\"}";
        User user2 = jsonMapper.userFromJSON(json);

        assertEquals(10, user2.getId());
        assertEquals("John Doe", user2.getFullName());
        assertEquals("john.doe", user2.getUserName());
        assertEquals("pass123", user2.getPassword());
    }

    @After
    public void tearDown() {
        jsonMapper = null;
        user = null;
    }
}