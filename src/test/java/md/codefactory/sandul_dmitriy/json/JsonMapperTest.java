package md.codefactory.sandul_dmitriy.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import md.codefactory.module4.domain.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonMapperTest {


    private final JsonMapper jsonMapper = new JsonMapper();

    @Test
    public void json_starts_with() throws Exception{
        JsonMapper mapper = jsonMapper;
        User user = new User("Vasea", "Popovici");
        String json = mapper.userToJson(user);
        assertTrue(json.startsWith("{"));
    }
    @Test
    public void jsonUserTest() throws Exception{
        String json = "{\"id\":10,\"fullName\":\"John\"} ";
        User user = jsonMapper.userFromJson(json);
        assertEquals(10, user.getId());
        assertEquals("John", user.getFullName());
    }
}